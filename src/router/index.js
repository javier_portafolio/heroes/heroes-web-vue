import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: { name: 'Login' }
  },
  {
    path: '/heroes',
    component: () => import(/* webpackChunkName: "heroes" */ '../views/Heroes/Index.vue'),
    meta: {
      requiresAuth: true
    },
    children: [
      {
        path: '',
        name: 'Heroes',
        component: () => import('../views/Heroes/Lista.vue')
      },
      {
        path: 'new',
        name: 'Heroes.crear',
        component: () => import('../views/Heroes/Crear.vue')
      },
      {
        path: ':codigo',
        name: 'Heroes.detalle',
        component: () => import('../views/Heroes/Detalle.vue')
      },
      {
        path: ':codigo/edit',
        name: 'Heroes.editar',
        component: () => import('../views/Heroes/Editar.vue')
      }
    ]
  },
  {
    path: '/dashboard',
    name: 'Tablero',
    component: () => import('../views/Dashboard.vue'),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Auth/Login.vue')
  },
  {
    path: '/register',
    name: 'Registro',
    component: () => import('../views/Auth/Registro.vue')
  },
  {
    path: '/forget',
    name: 'Forget',
    component: () => import('../views/Auth/Forget.vue')
  },
  {
    path: '/recovery',
    name: 'Recovery',
    component: () => import('../views/Auth/Recovery.vue')
  },
  {
    path: '/confirmacion',
    name: 'Confirmacion',
    component: () => import('../views/Auth/Confirmacion.vue')
  },
  {
    path: '*',
    name: 'NotFound',
    component: () => import('../views/Errors/404.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth)
  if (requiresAuth && !localStorage.getItem('is_loggedin')) {
    next('/login')
  }
  else {
    next()
  }
})

export default router
