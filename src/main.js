import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import VueAxios from 'vue-axios'
import Vuelidate from 'vuelidate'
import Swal from 'sweetalert2'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/js/bootstrap.bundle'
import '@fortawesome/fontawesome-free/css/all.min.css'
import moment from 'moment'

Vue.use(VueAxios, axios)
Vue.use(Vuelidate)
window.Swal = Swal
// Toast config
window.Toast= Swal.mixin({
  toast: true,
  position: 'top',
  showConfirmButton: false,
  timer: 4000,
})

// Agregamos la URL base de nuestra API
if (process.env.VUE_APP_HOST_API) {
  axios.defaults.baseURL = process.env.VUE_APP_HOST_API
}

let isRefreshing = false
let subscribers = []

function onAccessTokenFetched(token) {
  subscribers = subscribers.filter(callback => callback(token))
}

function addSubscriber(callback) {
  subscribers.push(callback)
}

axios.interceptors.response.use(response => {
  return response
}, error => {
  const { config, response: { data } } = error
  const originalRequest = config

  if (data.code === 401 && data.message === "Token expirado.") {
    if (!isRefreshing) {
      isRefreshing = true
      store.dispatch('usuario/refreshToken')
        .then(res => {
          isRefreshing = false
          store.dispatch('usuario/saveToken', res.data.body)
          onAccessTokenFetched(res.data.body.token)
        })
        .catch(err => {
          isRefreshing = false
          store.dispatch('usuario/clearStorage')
          Toast.fire({
            icon: 'error',
            title: err.response.data.message
          })
          router.push({name: 'Login'})
        })
    }

    const retryOriginalRequest = new Promise((resolve) => {
      addSubscriber(token => {
        originalRequest.headers.Authorization = `Bearer ${token}`
        resolve(axios(originalRequest))
      })
    })
    return retryOriginalRequest
  }

  if (data.message === "Token Signature verification failed") {
    store.dispatch('usuario/clearStorage')
    Toast.fire({
      icon: 'error',
      title: error.response.data.message
    })
    router.push({name: 'Login'})
  }

  return Promise.reject(error)
})

Vue.config.productionTip = false

Vue.filter('capitalize', function (value) {
  return value.replace(/(^\w{1})|(\s+\w{1})/g, letter => letter.toUpperCase())
})
Vue.filter('formatearFecha', function (value) {
  return moment(value).format('DD-MM-YYYY H:mm:ss')
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
