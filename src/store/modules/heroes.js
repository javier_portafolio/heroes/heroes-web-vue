import axios from 'axios'

export default {
  namespaced: true,
  state: {
    heroes: []
  },
  mutations: {
    setHeroes(state, payload) {
      state.heroes = payload
    }
  },
  actions: {
    setHeroes({commit}, body) {
      commit('setHeroes', body)
    }
  },
  getters: {
  }
}
