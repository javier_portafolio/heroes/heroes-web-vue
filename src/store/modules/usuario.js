import axios from 'axios'

export default {
  namespaced: true,
  state: {
    usuario: {},
    token: {}
  },
  mutations: {
    setToken(state, payload) {
      localStorage.setItem('token', payload.token);
      localStorage.setItem('refresh_token', payload.refresh_token);
      state.token = payload
    },
    setUsuario(state, payload) {
      localStorage.setItem('usuario', JSON.stringify(payload));
      state.usuario = payload
    },
    clearUsuario(state) {
      localStorage.clear()
      state.usuario = {}
    }
  },
  actions: {
    saveToken({commit}, body) {
      commit('setToken', body)
    },
    clearStorage() {
      localStorage.clear()
    },
    saveUsuario({commit}, body) {
      commit('setUsuario', body)
    },
    clearUsuario({commit}) {
      commit('clearUsuario')
    },
    refreshToken({state}) {
      let config = {
        headers: {
          'Accept': 'application/json',
          'Authorization': `Bearer ${state.token.token}`,
          'apikey': state.usuario.apikey
        }
      }
      let body = {
        refresh_token: `${state.token.refresh_token}`
      }
      return new Promise((resolve, reject) => {
        axios.post('/auth/refresh', body, config)
          .then(res => {
            resolve(res)
          })
          .catch(err => {
            reject(err)
          })
      })
    }
  },
  getters: {
  }
}
