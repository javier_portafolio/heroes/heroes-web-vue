import Vue from 'vue'
import Vuex from 'vuex'
import usuario from './modules/usuario';
import heroes from './modules/heroes'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    usuario,
    heroes
  }
})
